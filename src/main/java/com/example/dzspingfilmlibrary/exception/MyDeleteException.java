package com.example.dzspingfilmlibrary.exception;

public class MyDeleteException extends Exception{
    public MyDeleteException(String message) {
        super(message);
    }
}
