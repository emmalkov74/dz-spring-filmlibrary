package com.example.dzspingfilmlibrary.constants;

public interface FileDirectoriesConstants {
    String FILMS_UPLOAD_DIRECTORY = "files/films";
}
