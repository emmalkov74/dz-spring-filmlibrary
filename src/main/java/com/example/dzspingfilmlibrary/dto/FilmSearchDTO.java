package com.example.dzspingfilmlibrary.dto;

import com.example.dzspingfilmlibrary.filmlibraryproject.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String title;
    private String directorsFio;
    private Genre genre;
}
