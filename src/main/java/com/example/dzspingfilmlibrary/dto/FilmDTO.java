package com.example.dzspingfilmlibrary.dto;

import com.example.dzspingfilmlibrary.filmlibraryproject.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor

public class FilmDTO extends GenericDTO {
    private String title;
    private String premierYear;
    private String country;
    private Genre genre;
    private String onlineCopyPath;
    private Set<Long> directorsIds;
    private boolean isDeleted;
}
