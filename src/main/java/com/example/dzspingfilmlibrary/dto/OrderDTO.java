package com.example.dzspingfilmlibrary.dto;

import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class OrderDTO extends GenericDTO{

    private LocalDateTime rentDate;
    private Long filmId;
    private Long userId;
    private FilmDTO filmDTO;
}


