package com.example.dzspingfilmlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DzSpingFilmlibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(DzSpingFilmlibraryApplication.class, args);
    }

}
