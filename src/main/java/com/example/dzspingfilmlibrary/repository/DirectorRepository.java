package com.example.dzspingfilmlibrary.repository;

import com.example.dzspingfilmlibrary.filmlibraryproject.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
}
