package com.example.dzspingfilmlibrary.repository;

import com.example.dzspingfilmlibrary.filmlibraryproject.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
}
