package com.example.dzspingfilmlibrary.service;

import com.example.dzspingfilmlibrary.dto.FilmDTO;
import com.example.dzspingfilmlibrary.dto.OrderDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Order;
import com.example.dzspingfilmlibrary.mapper.OrderMapper;
import com.example.dzspingfilmlibrary.repository.OrderRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    private OrderRepository orderRepository;
    private final FilmService filmService;

    protected OrderService(OrderRepository orderRepository,
                           OrderMapper orderMapper,
                           FilmService filmService) {
        super(orderRepository, orderMapper);
        this.orderRepository = orderRepository;
        this.filmService = filmService;
    }
    public OrderDTO orderFilm(OrderDTO orderDTO) {

        orderDTO.setRentDate(LocalDateTime.now());
        orderDTO.setCreatedWhen(LocalDateTime.now());
        orderDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(orderDTO)));
    }
}
