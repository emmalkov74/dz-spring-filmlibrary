package com.example.dzspingfilmlibrary.service;

import com.example.dzspingfilmlibrary.dto.FilmDTO;
import com.example.dzspingfilmlibrary.dto.FilmSearchDTO;
import com.example.dzspingfilmlibrary.dto.FilmWithDirectorsDTO;
import com.example.dzspingfilmlibrary.exception.MyDeleteException;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Film;
import com.example.dzspingfilmlibrary.mapper.FilmMapper;
import com.example.dzspingfilmlibrary.mapper.FilmWithDirectorsMapper;
import com.example.dzspingfilmlibrary.repository.FilmRepository;
import com.example.dzspingfilmlibrary.utils.FileHelper;
import org.springframework.data.domain.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;


import java.time.LocalDateTime;
import java.util.List;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {
    //  Инжектим конкретный репозиторий для работы с таблицей books
    private final FilmRepository repository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository repository,
                          FilmMapper mapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        //Передаем этот репозиторй в абстрактный севрис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO,
                                              Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = repository.searchFilms(genre,
                filmSearchDTO.getTitle(),
                filmSearchDTO.getDirectorsFio(),
                pageable
        );
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    // files/books/year/month/day/file_name_{id}_{created_when}.txt
    // files/книга_id.pdf
    public FilmDTO create(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);

        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public FilmDTO update(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    @Override
    public void delete(Long id) throws MyDeleteException {
        Film film = repository.findById(id).orElseThrow(
                () -> new NotFoundException("" +
                        "Фильма с заданным ID=" + id + " не существует"));
        //TODO: сделать проверку, что нет АКТИВНЫХ аренд!
        if (film.getOrders().size() > 0) {
            throw new MyDeleteException("book cannot be deleted");
            //TODO: разобраться почему не хочет отображать русские буквы!
//            throw new MyDeleteException("Книга не может быть удалена, так как у нее есть активные аренды");
        } else {
            repository.deleteById(film.getId());
        }
    }



}
