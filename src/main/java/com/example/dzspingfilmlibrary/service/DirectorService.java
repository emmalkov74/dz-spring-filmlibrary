package com.example.dzspingfilmlibrary.service;

import com.example.dzspingfilmlibrary.dto.DirectorDTO;
import com.example.dzspingfilmlibrary.dto.DirectorWithFilmsDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Director;
import com.example.dzspingfilmlibrary.mapper.DirectorMapper;
import com.example.dzspingfilmlibrary.mapper.DirectorWithFilmsMapper;
import com.example.dzspingfilmlibrary.repository.DirectorRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DirectorService  extends GenericService<Director, DirectorDTO> {

    private final DirectorRepository directorRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;
    private final DirectorMapper directorMapper;

    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(directorRepository, directorMapper);
        this.directorRepository = directorRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
        this.directorMapper = directorMapper;
    }

//    public DirectorDTO addFilm(Long filmId, Long directorId) {
//        DirectorDTO directorDTO = directorMapper.toDto(directorRepository.findById(directorId)
//                .orElseThrow(() -> new NotFoundException("Данных по заданному id: " + directorId + " не найдены")));
//        directorDTO.getFilmIds().add(filmId);
//        return directorMapper.toDto(directorRepository.save(mapper.toEntity(directorDTO)));
//    }

    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms() {
        return directorWithFilmsMapper.toDTOs(directorRepository.findAll());
    }
}
