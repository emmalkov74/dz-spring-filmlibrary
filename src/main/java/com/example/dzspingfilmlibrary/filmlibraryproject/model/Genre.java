package com.example.dzspingfilmlibrary.filmlibraryproject.model;

public enum Genre {
    COMEDY("Комедия"),
    DRAMA("Драма"),
    ACTION("Боевик"),
    DETECTIVE("Детектив"),
    HORROR("Ужасы");

    private final String genreDisplayValue;

    Genre(String genreDisplayValue) {
        this.genreDisplayValue = genreDisplayValue;
    }

    public String getGenreDisplayValue() {
        return genreDisplayValue;
    }
}
