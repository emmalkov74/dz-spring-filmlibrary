package com.example.dzspingfilmlibrary.filmlibraryproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table( name = "films")
@Setter
@Getter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "films_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Film
    extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "premier_year", nullable = false)
    private LocalDate premierYear;
    @Column(name = "country", nullable = false)
    private String country;
    @Column(name = "online_copy_path")
    private String onlineCopyPath;;
    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;
    @ManyToMany
    @JoinTable(
            name = "film_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS")
    )
    private Set<Director> directors;

    @OneToMany(mappedBy = "film")
    private Set<Order> orders;

}
