package com.example.dzspingfilmlibrary.mapper;
import com.example.dzspingfilmlibrary.dto.DirectorWithFilmsDTO;
import com.example.dzspingfilmlibrary.dto.FilmWithDirectorsDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Director;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Film;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.GenericModel;
import com.example.dzspingfilmlibrary.repository.FilmRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorWithFilmsMapper extends GenericMapper<Director, DirectorWithFilmsDTO> {
    private final FilmRepository filmRepository;

    public DirectorWithFilmsMapper(ModelMapper mapper,
                                   FilmRepository filmRepository) {
        super(mapper, Director.class, DirectorWithFilmsDTO.class);
        this.filmRepository = filmRepository;
    }


    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorWithFilmsDTO.class)
                .addMappings(m -> m.skip(DirectorWithFilmsDTO::setFilmIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(DirectorWithFilmsDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());

    }


    @Override
    protected void mapSpecificFields(DirectorWithFilmsDTO source, Director destination) {
        destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmIds())));
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorWithFilmsDTO destination) {
        destination.setFilmIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Director entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());

    }

}
