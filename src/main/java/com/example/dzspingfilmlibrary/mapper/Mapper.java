package com.example.dzspingfilmlibrary.mapper;

import com.example.dzspingfilmlibrary.dto.GenericDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.GenericModel;

import java.util.List;

public interface Mapper <E extends GenericModel, D extends GenericDTO>{

        E toEntity(D dto);

        List<E> toEntities(List<D> dtos);

        D toDTO(E entity);

        List<D> toDTOs(List<E> entities);


}
