package com.example.dzspingfilmlibrary.MVC.controller;

import com.example.dzspingfilmlibrary.dto.*;
import com.example.dzspingfilmlibrary.exception.MyDeleteException;
import com.example.dzspingfilmlibrary.service.DirectorService;
import com.example.dzspingfilmlibrary.service.FilmService;
import com.example.dzspingfilmlibrary.service.OrderService;
import com.example.dzspingfilmlibrary.service.UserService;
import com.example.dzspingfilmlibrary.service.userdetails.CustomUserDetails;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Hidden
@Controller
@RequestMapping("films")
@Slf4j
public class MVCFilmController {
    private final FilmService filmService;
    private final DirectorService directorService;

    private final UserService userService;
    private final OrderService orderService;

    public MVCFilmController(FilmService filmService,
                             DirectorService directorService,
                             UserService userService, OrderService orderService) {

        this.filmService = filmService;
        this.directorService = directorService;
        this.userService = userService;
        this.orderService = orderService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<FilmWithDirectorsDTO> result = filmService.getAllFilmsWithDirectors(pageRequest);
        model.addAttribute("films", result);
        return "films/viewAllFilms";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmService.getFilmWithDirectors(id));
        return "films/viewFilm";
    }


    @GetMapping("/add")
    public String create(Model model) {
        List<DirectorDTO> directors = directorService.listAll();
        model.addAttribute("directors", directors);
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @ModelAttribute("director") Long directorId,
                         @RequestParam MultipartFile file) {
        Set<Long> filmDirectors = new HashSet<>();
        filmDirectors.add(directorId);
        filmDTO.setDirectorsIds(filmDirectors);
        log.error(filmDTO.toString());
        if (file != null && file.getSize() > 0) {
            filmService.create(filmDTO, file);
        }
        else {
            filmService.create(filmDTO);
        }
        return "redirect:/films";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        List<DirectorDTO> directors = directorService.listAll();
        model.addAttribute("directors", directors);
        model.addAttribute("film", filmService.getOne(id));
        return "films/updateFilm";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @ModelAttribute("director") Long directorId,
                         @RequestParam MultipartFile file) {
        Set<Long> filmDirectors = new HashSet<>();
        filmDirectors.add(directorId);
        filmDTO.setDirectorsIds(filmDirectors);
        if (file != null && file.getSize() > 0) {
            filmService.update(filmDTO, file);
        }
        else {
            filmService.update(filmDTO);
        }
        return "redirect:/films";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        try {
            filmService.delete(id);
        }
        catch (MyDeleteException e) {
            log.error("MVCBookController#delete(): {}", e.getMessage());
            return "redirect:/error/error-message?message=" + e.getLocalizedMessage();
        }
        return "redirect:/films";
    }

    @PostMapping("/search")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("filmSearchForm") FilmSearchDTO filmSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("films", filmService.findFilms(filmSearchDTO, pageRequest));
        return "films/viewAllFilms";
    }

    @PostMapping("/search/director")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("directorSearchForm") DirectorDTO directorDTO,
                              Model model) {
        FilmSearchDTO filmSearchDTO = new FilmSearchDTO();
        filmSearchDTO.setDirectorsFio(directorDTO.getDirectorsFio());
        return searchFilms(page, pageSize, filmSearchDTO, model);
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadBook(@Param(value = "filmId") Long filmId) throws IOException {
        FilmDTO filmDTO = filmService.getOne(filmId);
        Path path = Paths.get(filmDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setUserId(Long.valueOf(customUserDetails.getUserId()));
        orderDTO.setFilmId(filmId);
        orderDTO.setRentDate(LocalDateTime.now());
        orderService.create(orderDTO);

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

}
