package com.example.dzspingfilmlibrary.REST.controller;

import com.example.dzspingfilmlibrary.dto.DirectorDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Director;
import com.example.dzspingfilmlibrary.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/directors")
@Tag(name = "Режисеры",
        description = "Контроллер для работы с режисерами фильмов фильмотеки")


public class DirectorController extends GenericController<Director, DirectorDTO>{

    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

//
//    @Operation(description = "Добавить фильм к режисеру", method = "addFilm")
//    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<DirectorDTO> addAuthor(@RequestParam(value = "filmId") Long filmId,
//                                            @RequestParam(value = "directorId") Long directorId) {
//        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(filmId, directorId));

//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
//        Author author = genericRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
//        author.getBooks().add(book);
//        return ResponseEntity.status(HttpStatus.OK).body(genericRepository.save(author));
//    }

//    }

}
