package com.example.dzspingfilmlibrary.REST.controller;

import com.example.dzspingfilmlibrary.dto.OrderDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Order;
import com.example.dzspingfilmlibrary.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/orders")
@Tag(name = "Просмотр фильмов",
        description = "Контроллер для работы с предоставлением фильмов пользователям фильмотеки")

public class OrderController extends GenericController<Order, OrderDTO>{

    private final OrderService orderService;
    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }
//    @Operation(description = "Продать фильм заданому пользователю", method = "addOrder")
//    @RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<OrderDTO> addOrder(@RequestParam(value = "filmId") Long filmId,
//                                               @RequestParam(value = "userId") Long userId) {
//        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.addOrder(filmId, userId));
//    }

}
