package com.example.dzspingfilmlibrary.REST.controller;
import com.example.dzspingfilmlibrary.dto.FilmDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Film;
import com.example.dzspingfilmlibrary.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO>{

    private final FilmService filmService;
    public FilmController(FilmService filmService) {
        super(filmService);
        this.filmService = filmService;
    }

//    @Operation(description = "Добавить режисера к фильму", method = "addDirector")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                               @RequestParam(value = "directorId") Long directorId) {
//        return ResponseEntity.status(HttpStatus.CREATED).body(filmService.addDirector(filmId, directorId));
//    }
//
//    @Operation(description = "Список всех арендованных/купленных фильмов у пользователя", method = "getAllUserFilms")
//    @RequestMapping(value = "/getAllUserFilms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<List<FilmDTO>> getAllUserFilms(@RequestParam(value = "userId") Long userId) {
//        return ResponseEntity.status(HttpStatus.OK).body(filmService.getAllUsersFilms( userId));
//    }

}
