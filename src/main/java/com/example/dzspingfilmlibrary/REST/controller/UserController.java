package com.example.dzspingfilmlibrary.REST.controller;

import com.example.dzspingfilmlibrary.dto.UserDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.User;
import com.example.dzspingfilmlibrary.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями фильмотеки")
public class UserController
        extends GenericController<User, UserDTO> {

    public UserController(UserService userService) {
        super(userService);
    }

}
