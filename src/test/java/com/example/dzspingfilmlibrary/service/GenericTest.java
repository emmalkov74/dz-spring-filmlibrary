package com.example.dzspingfilmlibrary.service;

import com.example.dzspingfilmlibrary.dto.GenericDTO;
import com.example.dzspingfilmlibrary.exception.MyDeleteException;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.GenericModel;
import com.example.dzspingfilmlibrary.service.userdetails.CustomUserDetails;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO> {

    protected GenericService<E, D> service;
    @BeforeEach
    void init(){
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                CustomUserDetails.builder().username("USER"), null, null);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    protected abstract void getAll();
    protected abstract void create();
    protected abstract void getOne();
    protected abstract void update();
    protected abstract void delete() throws MyDeleteException;

}
