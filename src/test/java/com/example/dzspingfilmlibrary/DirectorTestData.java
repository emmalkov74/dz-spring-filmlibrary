package com.example.dzspingfilmlibrary;

import com.example.dzspingfilmlibrary.dto.DirectorDTO;
import com.example.dzspingfilmlibrary.filmlibraryproject.model.Director;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DirectorTestData {

    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO(
            "directorFio1",
            "position1",
            new HashSet<>()
    );

    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO(
            "directorFio2",
            "position2",
            new HashSet<>()
    );

    DirectorDTO DIRECTOR_DTO_3_DELETED = new DirectorDTO(
            "directorFio3",
            "position3",
            new HashSet<>()
    );

    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1,
            DIRECTOR_DTO_2,
            DIRECTOR_DTO_3_DELETED);

    Director DIRECTOR_1 = new Director(
            "directorFio1",
            "position1",
            null
    );

    Director DIRECTOR_2 = new Director(
            "directorFio2",
            "position2",
            null
    );

    Director DIRECTOR_3 = new Director(
            "directorFio3",
            "position3",
            null
    );

    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1,
            DIRECTOR_2,
            DIRECTOR_3);

}
